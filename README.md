# ds-os

This repository contains seven submodules and has been adapted from cuteradio. Each submodule corresponds to a Yocto layer and is added as a subdirectory to the directory _ds-os/sources_. DS-OS is currently based on the Yocto release _warrior_.

In order to have a functional enviroment for building:

Clone the main repository with all its submodules.

    user@alkaid:~$ git clone --recurse-submodules git@bitbucket.org:deepseaiot/ds-os.git

If we need to clone the repository to a specific tag (e.g v0.7.1)

    user@alkaid:~$ git clone -b v0.7.1 --recurse-submodules git@bitbucket.org:deepseaiot/ds-os.git

After all source files have being downloaded enter the ds-os/ directory.

    user@alkaid:~$ cd ds-os/

Next step is to configure the build directory. To build the ds-os image (core-image-base-ds) we need to configure yocto layers and conf file (_bblayers.conf_ and _local.conf_) from ds-os template files in the build directory.

    user@alkaid:~/ds-os$ TEMPLATECONF=../sources/meta-ds/conf . sources/poky/oe-init-build-env

After the above command, yocto will create a build directory under the current path and will enter it. It will also create the conf directory under (_ds-os/build/conf_). In addition it will create the _ds-os/build/conf/bblayers.conf_ and _ds-os/build/conf/local.conf_ using the given TEMPLATECONF. 

** NOTE: **
_The above command is only needed on a fresh installation or if the build directory does not exist. if it already exists we only need to source the poky enviroment file since the conf and layers file are already configured_ (E.g _. sources/poky/oe-init-build-env_) 
    
Now we can build the ds-os image using bitbake:

    user@alkaid:~/ds-os/build/$bitbake core-image-base-ds

This will build the image using for default MACHINE noted in _ds-os/build/conf/local.conf_ file. If we need to build for a different MACHINE use:

    user@alkaid:~/ds-os/build/$MACHINE=ds-stratocan bitbake core-image-base-ds

